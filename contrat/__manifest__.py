{
    'name': "Contrat Module",
    'sequence': -100,
    'application': True,
    'version': '1.0',
    'license': "LGPL-3",
    'depends': ['base', 'promoteurinitiative'],
    'author': "Farid Adwani",
    'category': 'Contrat Category',
    'description': "this is a description for contrat module",
    'data': [
        'rules/ir.model.access.csv',
        'views/contrat.xml',
        'reports/report.xml',
        'reports/contrat.xml',
        'reports/email_template.xml',

    ],
    'demo': [
        # 'demo/demo_data.xml',
    ],
}
