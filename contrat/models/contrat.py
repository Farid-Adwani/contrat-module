import base64
import re

from datetime import datetime, date
from odoo import models, fields, api, exceptions, _
from odoo.exceptions import ValidationError, UserError


class TestModel(models.Model):
    _name = "initiative.contrat"
    _description = "Contrat Model"

    nom = fields.Char("Nom", required=True)
    prenom = fields.Char(string="Prénom", required=True)
    delegation_id = fields.Many2one('res.delegation', string='Délégation', required=True)
    sexe = fields.Selection(
        [('homme', 'Homme'), ('femme', 'Femme')
         ], string="Sexe", required=True)
    date = fields.Date(
        string='Date',
        default=fields.Date.context_today,
        required=True
    )
    cin = fields.Char(string="CIN", size=8, required=True)
    civilite = fields.Selection(
        [('monsieur', 'Monsieur'), ('madame', 'Madame'), ('mademoiselle', 'Mademoiselle')
         ], string="Civilité", required=True)
    email = fields.Char(required=True)
    tel = fields.Char(string="Téléphone", size=8, required=True)
    etat = fields.Selection(
        [('signe', 'Signé'), ('non_signe', 'Non signé')
         ], string="Etat", required=True,
        default='non_signe',
    )
    promoteur = fields.Many2one('initiative.promoteur', string='Promoteur')

    @api.onchange('promoteur')
    def onchange_promoteur(self):
        self.nom = self.promoteur.nom
        self.prenom = self.promoteur.prenom
        self.cin = self.promoteur.cin
        self.civilite = self.promoteur.civilite
        self.delegation_id = self.promoteur.delegation_id
        self.email = self.promoteur.email
        self.sexe = self.promoteur.sexe
        self.tel = self.promoteur.tel

    def send_email(self):
        template_id = self.env.ref('contrat.contrat_email_template').id
        template = self.env['mail.template'].browse(template_id)
        template.send_mail(self.id, force_send=True)
        print("Done sending email")
